# Build image
FROM golang:1.17

# Install golangci-lint
RUN curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b /usr/local/bin v1.39.0

# Set the working directory
WORKDIR /src

# Install dependencies
COPY . /src
RUN go get
