package trakt

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	traktURL       = "https://api.trakt.tv"
	requestTimeout = 30 * time.Second
)

// Client wraps connection parameters for the Trakt.tv API.
type Client struct {
	baseURL        string
	requestTimeout time.Duration
	clientID       string
	clientSecret   string
	accessToken    string
}

// ClientOptions contains auth and connection details for the Trakt.tv API.
type ClientOptions struct {
	RequestTimeout time.Duration
	ClientID       string
	ClientSecret   string
	AccessToken    string
}

// NewClient returns new Trakt.tv client.
func NewClient(options ClientOptions) *Client {
	defaultOptions := ClientOptions{}

	client := Client{
		baseURL:        traktURL,
		requestTimeout: requestTimeout,
		clientID:       options.ClientID,
		clientSecret:   options.ClientSecret,
		accessToken:    options.AccessToken,
	}

	if options.RequestTimeout != defaultOptions.RequestTimeout {
		client.requestTimeout = options.RequestTimeout
	}

	return &client
}

func (c *Client) performRequest(method string, path string, body io.Reader, result interface{}) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), requestTimeout)
	defer ctxCancel()

	req, err := http.NewRequestWithContext(ctx, method, fmt.Sprintf("%s/%s", traktURL, path), body)
	if err != nil {
		return newError("failed to build request", method, fmt.Sprintf("%s/%s", c.baseURL, path), 0, false, err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", fmt.Sprintf("Go-Trakt v%s", Version))
	req.Header.Set("trakt-api-version", "2")
	req.Header.Set("trakt-api-key", c.clientID)

	if c.accessToken != "" {
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.accessToken))
	}

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return newError("failed to perform request", method, fmt.Sprintf("%s/%s", c.baseURL, path), 0, false, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		return newError("", method, fmt.Sprintf("%s/%s", c.baseURL, path), resp.StatusCode,
			resp.StatusCode == http.StatusTooManyRequests, nil)
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return newError("failed to read response body", method, fmt.Sprintf("%s/%s", c.baseURL, path), resp.StatusCode,
			false, err)
	}

	err = json.Unmarshal(respBody, result)
	if err != nil {
		return newError("failed to parse response body JSON", method, fmt.Sprintf("%s/%s", c.baseURL, path),
			resp.StatusCode, false, err)
	}

	return nil
}

func (c *Client) getRequest(path string, result interface{}) error {
	return c.performRequest(http.MethodGet, path, bytes.NewBuffer([]byte{}), result)
}

func (c *Client) postRequest(path string, body, result interface{}) error {
	b, err := json.Marshal(body)
	if err != nil {
		return newError("failed to convert request body to JSON", http.MethodPost, fmt.Sprintf("%s/%s", c.baseURL, path), 0,
			false, err)
	}

	return c.performRequest(http.MethodPost, path, bytes.NewBuffer(b), result)
}
