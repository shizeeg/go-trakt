package trakt

import (
	"fmt"
	"net/http"
	"strings"
)

var (
	errRemovalNotConfirmed     = fmt.Errorf("removal was not confirmed")
	errAddingNotConfirmed      = fmt.Errorf("adding was not confirmed")
	errAuthFailed              = fmt.Errorf("")
	errTypeUnsupported         = fmt.Errorf("unsupported type: ")
	errScrobbleTypeUnsupported = fmt.Errorf("unsupported type for scrobble: ")
	errItemTypeInvalid         = fmt.Errorf("invalid item type: ")
)

type Error struct {
	Message           string
	Method            string
	URL               string
	ResponseCode      int
	RateLimitExceeded bool
	Parent            error
}

func newError(msg, method, url string, responseCode int, rateLimitExceeded bool, parent error) *Error {
	return &Error{
		Message:           msg,
		Method:            method,
		URL:               url,
		ResponseCode:      responseCode,
		RateLimitExceeded: rateLimitExceeded,
		Parent:            parent,
	}
}

func (e *Error) Error() string {
	parts := []string{}

	if e.Method != "" && e.URL != "" {
		if e.ResponseCode != 0 {
			parts = append(parts, fmt.Sprintf("%s %s failed with status %d (%s)", e.Method, e.URL, e.ResponseCode,
				http.StatusText(e.ResponseCode)))
		} else {
			parts = append(parts, fmt.Sprintf("%s %s failed", e.Method, e.URL))
		}
	}

	if e.Message != "" {
		parts = append(parts, e.Message)
	}

	if e.Parent != nil {
		parts = append(parts, e.Parent.Error())
	}

	return strings.Join(parts, ": ")
}
