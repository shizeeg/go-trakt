package trakt

import (
	"fmt"
)

// ScrobbleService services wrapping the scrobble scope.
type ScrobbleService struct {
	Client *Client
}

type scrobbleMovieRequest struct {
	Movie    Movie   `json:"movie"`
	Progress float32 `json:"progress"`
}

type scrobbleEpisodeRequest struct {
	Episode  Episode `json:"episode"`
	Progress float32 `json:"progress"`
}

// Scrobble creates a new ScrobbleService.
func (c *Client) Scrobble() *ScrobbleService {
	return &ScrobbleService{Client: c}
}

// Start watching in a media center.
func (s *ScrobbleService) Start(item *Item, progress float32) (*ScrobbleItem, error) {
	return s.request("start", item, progress)
}

// Pause watching in a media center.
func (s *ScrobbleService) Pause(item *Item, progress float32) (*ScrobbleItem, error) {
	return s.request("pause", item, progress)
}

// Stop watching in a media center.
func (s *ScrobbleService) Stop(item *Item, progress float32) (*ScrobbleItem, error) {
	return s.request("stop", item, progress)
}

func (s *ScrobbleService) request(action string, item *Item, progress float32) (*ScrobbleItem, error) {
	var result ScrobbleItem

	var request interface{}

	switch item.Type {
	case ItemTypeMovie:
		request = scrobbleMovieRequest{Movie: item.Movie, Progress: progress}
	case ItemTypeEpisode:
		request = scrobbleEpisodeRequest{Episode: item.Episode, Progress: progress}
	case ItemTypeShow:
		return nil, newError(string(item.Type), "", "", 0, false, errScrobbleTypeUnsupported)
	case ItemTypeAll:
		return nil, newError(string(item.Type), "", "", 0, false, errScrobbleTypeUnsupported)
	default:
		return nil, newError(string(item.Type), "", "", 0, false, errScrobbleTypeUnsupported)
	}

	err := s.Client.postRequest(fmt.Sprintf("scrobble/%s", action), &request, &result)
	if err != nil {
		return &result, err
	}

	return &result, nil
}
