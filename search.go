package trakt

import (
	"encoding/json"
	"fmt"
	"strings"
)

// SearchService services wrapping the search scope.
type SearchService struct {
	Client *Client
}

// Search creates a new SearchService.
func (c *Client) Search() *SearchService {
	return &SearchService{Client: c}
}

// IDLookup find items by service id and optional type.
func (s *SearchService) IDLookup(serviceType ServiceType, itemID string, itemType ItemType) ([]*Item, error) {
	var result []*Item

	err := s.Client.getRequest(fmt.Sprintf("search/%s/%s?type=%s", serviceType, itemID, itemType), &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

// TitleLookup find items by guessit json output
func (s *SearchService) TitleLookup(GuessPayload []byte) (items []*Item, err error) {
	var kv map[string]interface{}
	json.Unmarshal(GuessPayload, &kv)
	title := kv["title"].(string)
	itemType := kv["type"].(string)
	if itemType == "episode" {
		itemType = "show"
	}
	if err = s.Client.getRequest(fmt.Sprintf("search/%s?field=title&query=%s", itemType, title), &items); err != nil {
		return items, err
	}

	for _, v := range items {
		if v.Type == "show" && strings.EqualFold(v.Show.Title, title) {
			seasonNumber := uint16(kv["season"].(float64))
			episodeNumber := uint16(kv["episode"].(float64))
			sss := ShowSeasonService{
				Client:       s.Client,
				ShowID:       v.TraktID(),
				SeasonNumber: seasonNumber,
			}
			episode, err := sss.Episode(episodeNumber).Summary()
			if err != nil {
				return items, err
			}
			v.Episode = *episode
			v.Type = ItemTypeEpisode
			return append(items, v), err
		} else if v.Type == "movie" {
			year := uint16(kv["year"].(float64))
			if v.Movie.Year == year {
				fmt.Printf("add to queue: %v\n", v)
				return append(items, v), nil
			}
		}
	}
	// log.Fatalln("nothing found")
	return
}
