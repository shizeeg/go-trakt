package trakt

// ShowService services wrapping the shows scope for a single show.
type ShowService struct {
	Client *Client
	ShowID uint64
}

// Show creates a new ShowService.
func (c *Client) Show(showID uint64) *ShowService {
	return &ShowService{Client: c, ShowID: showID}
}
