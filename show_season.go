package trakt

// ShowSeasonService services wrapping the seasons scope for a single season within the shows scope for a single show.
type ShowSeasonService struct {
	Client       *Client
	ShowID       uint64
	SeasonNumber uint16
}

// Season creates a new ShowSeasonService.
func (s *ShowService) Season(seasonNumber uint16) *ShowSeasonService {
	return &ShowSeasonService{Client: s.Client, ShowID: s.ShowID, SeasonNumber: seasonNumber}
}
