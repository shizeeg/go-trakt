package trakt

import (
	"fmt"
)

// ShowSeasonEpisodeService services wrapping the episodes scope for a single episode within the seasons scope for a
// single season within the shows scope for a single show.
type ShowSeasonEpisodeService struct {
	Client        *Client
	ShowID        uint64
	SeasonNumber  uint16
	EpisodeNumber uint16
}

// Episode creates a new ShowSeasonEpisodeService.
func (s *ShowSeasonService) Episode(episodeNumber uint16) *ShowSeasonEpisodeService {
	return &ShowSeasonEpisodeService{
		Client:        s.Client,
		ShowID:        s.ShowID,
		SeasonNumber:  s.SeasonNumber,
		EpisodeNumber: episodeNumber,
	}
}

// Summary summary of an episode.
func (s *ShowSeasonEpisodeService) Summary() (*Episode, error) {
	var result *Episode

	err := s.Client.getRequest(
		fmt.Sprintf("shows/%d/seasons/%d/episodes/%d?extended=full", s.ShowID, s.SeasonNumber, s.EpisodeNumber),
		&result)
	if err != nil {
		return result, err
	}

	return result, nil
}
