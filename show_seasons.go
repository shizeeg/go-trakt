package trakt

import (
	"fmt"
)

// ShowSeasonsService services wrapping the seasons scope within the shows scope for a single show.
type ShowSeasonsService struct {
	Client *Client
	ShowID uint64
}

// Seasons creates a new ShowSeasonsService.
func (s *ShowService) Seasons() *ShowSeasonsService {
	return &ShowSeasonsService{Client: s.Client, ShowID: s.ShowID}
}

// Summary of all seasons of a show.
func (s *ShowSeasonsService) Summary() ([]*Season, error) {
	var result []*Season

	err := s.Client.getRequest(fmt.Sprintf("shows/%d/seasons?extended=full,episodes", s.ShowID), &result)
	if err != nil {
		return result, err
	}

	return result, nil
}
