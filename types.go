package trakt

import (
	"fmt"
	"time"
)

// ServiceType type of service.
type ServiceType string

// Service types.
const (
	ServiceTypeTrakt ServiceType = "trakt"
	ServiceTypeIMDB  ServiceType = "imdb"
	ServiceTypeTMDB  ServiceType = "tmdb"
	ServiceTypeTVDB  ServiceType = "tvdb"
)

// ItemType type of item.
type ItemType string

// Item types.
const (
	ItemTypeAll     ItemType = ""
	ItemTypeMovie   ItemType = "movie"
	ItemTypeShow    ItemType = "show"
	ItemTypeEpisode ItemType = "episode"
)

// Key identifies an item by type and id.
type Key struct {
	Type ItemType `json:"type"`
	ID   uint64   `json:"id"`
}

func (k *Key) String() string {
	if k.Type == ItemTypeAll {
		return ""
	}

	return fmt.Sprintf("%s.%d", k.Type, k.ID)
}

// Equal compares two keys for equality.
func (k *Key) Equal(k2 *Key) bool {
	return k.Type == k2.Type && k.ID == k2.ID
}

// Movie represents a movie.
type Movie struct {
	Title string `json:"title"`
	Year  uint16 `json:"year"`
	IDs   struct {
		Slug  string `json:"slug"`
		Trakt uint64 `json:"trakt"`
		Imdb  string `json:"imdb"`
		Tmdb  uint64 `json:"tmdb"`
	} `json:"ids"`
}

// Show represents a show.
type Show struct {
	Title string `json:"title"`
	Year  uint16 `json:"year"`
	IDs   struct {
		Slug  string `json:"slug"`
		Trakt uint64 `json:"trakt"`
		TvDB  uint64 `json:"tvdb"`
		Tmdb  uint64 `json:"tmdb"`
		Imdb  string `json:"imdb"`
	} `json:"ids"`
}

// Season represents a season.
type Season struct {
	Number   uint16    `json:"number"`
	Episodes []Episode `json:"episodes"`
	IDs      struct {
		Trakt uint64 `json:"trakt"`
		TvDB  uint64 `json:"tvdb"`
		Tmdb  uint64 `json:"tmdb"`
	} `json:"ids"`
}

// Episode represents an episode.
type Episode struct {
	Season     uint16    `json:"season"`
	Number     uint16    `json:"number"`
	Title      string    `json:"title"`
	FirstAired time.Time `json:"first_aired"`
	IDs        struct {
		Trakt uint64 `json:"trakt"`
		TvDB  uint64 `json:"tvdb"`
		Tmdb  uint64 `json:"tmdb"`
		Imdb  string `json:"imdb"`
	} `json:"ids"`
}

// Item a movie, show, episode, person or list.
type Item struct {
	Type    ItemType `json:"type"`
	Movie   Movie    `json:"movie"`
	Show    Show     `json:"show"`
	Episode Episode  `json:"episode"`
}

// TraktID returns the Trakt.tv id of an item.
func (i *Item) TraktID() uint64 {
	switch i.Type {
	case ItemTypeMovie:
		return i.Movie.IDs.Trakt
	case ItemTypeShow:
		return i.Show.IDs.Trakt
	case ItemTypeEpisode:
		return i.Episode.IDs.Trakt
	case ItemTypeAll:
		return uint64(0)
	default:
		return uint64(0)
	}
}

// TraktKey Trakt.tv key (type and id).
func (i *Item) TraktKey() *Key {
	return &Key{Type: i.Type, ID: i.TraktID()}
}

func (i *Item) String() string {
	switch i.Type {
	case ItemTypeMovie:
		return fmt.Sprintf("%s (%04d)", i.Movie.Title, i.Movie.Year)
	case ItemTypeShow:
		return fmt.Sprintf("%s (%04d)", i.Show.Title, i.Show.Year)
	case ItemTypeEpisode:
		if i.Show.IDs.Trakt > 0 {
			return fmt.Sprintf("%s (%04d) %dx%02d %s",
				i.Show.Title, i.Show.Year, i.Episode.Season, i.Episode.Number, i.Episode.Title)
		}

		return fmt.Sprintf("%dx%02d %s", i.Episode.Season, i.Episode.Number, i.Episode.Title)
	case ItemTypeAll:
		return fmt.Sprintf("%q", *i)
	default:
		return fmt.Sprintf("%q", *i)
	}
}

// HistoryItem a movie, show, season or episode from watch histroy.
type HistoryItem struct {
	Item

	ID        uint64    `json:"id"`
	WatchedAt time.Time `json:"watched_at"`
	Action    string    `json:"action"`
}

// ScrobbleItem a movie, show, season or episode from a scrobble response.
type ScrobbleItem struct {
	Item

	ID       uint64  `json:"id"`
	Action   string  `json:"action"`
	Progress float64 `json:"progress"`
}
